(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./hello-world"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var hello_world_1 = require("./hello-world");
    // debugger;
    hello_world_1.helloWorld();
});
// var i = 1;
// setInterval(function(){
//     console.log('奇数i:'+i);
//     i+=2;
// },3000);
//# sourceMappingURL=index.js.map