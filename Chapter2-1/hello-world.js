(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.helloWorld = void 0;
    function helloWorld() {
        console.log('hello, world.');
        var i = 1;
        var j = 2;
        var c = i + j;
        console.log("1+2=" + c);
        // var i=0;
        // setInterval(function(){
        //     console.log('偶数i:'+i);
        //     i+=2;
        // },3000);
    }
    exports.helloWorld = helloWorld;
});
//# sourceMappingURL=hello-world.js.map