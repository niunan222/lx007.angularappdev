"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PetService = void 0;
//src\pet.service.ts 定义宠物服务类
var PetService = /** @class */ (function () {
    function PetService() {
        //宠物数组，模拟的宠物数据
        this.pets = [
            { family: "狗", name: "旺财", price: 99.9 },
            { family: "猫", name: "汤姆", price: 28.5 },
        ];
    }
    //获取宠物数量
    PetService.prototype.getPetCount = function () {
        return this.pets.length;
    };
    //获取宠物数组
    PetService.prototype.getPets = function () {
        return this.pets;
    };
    return PetService;
}());
exports.PetService = PetService;
//# sourceMappingURL=pet.service.js.map