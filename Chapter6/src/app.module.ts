//src\app.module.ts 根模块

import { NgModule } from "@angular/core";
import {BrowserModule} from '@angular/platform-browser'
import { AppComponent } from "./app.component";

@NgModule({
    imports:[BrowserModule],//导入浏览器模块
    declarations:[AppComponent], //声明组件appcomponent为当前模块内的组件
    bootstrap:[AppComponent] //从组件appcomponent启动
})
export class AppModule{

}