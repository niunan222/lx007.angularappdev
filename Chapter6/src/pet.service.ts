//src\pet.service.ts 定义宠物服务类
export class PetService{
    //宠物数组，模拟的宠物数据
    private pets:Array<{family:string,name:string,price:number}> = [
        {family:"狗",name:"旺财",price:99.9},
        {family:"猫",name:"汤姆",price:28.5},
    ];

    //获取宠物数量
    getPetCount():number{
        return this.pets.length;
    }

    //获取宠物数组
    getPets():Array<{family:string,name:string,price:number}> {
        return this.pets;
    }
}