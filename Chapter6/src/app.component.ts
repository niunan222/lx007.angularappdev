//src\app.component.ts 根组件

import { Component } from "@angular/core";
import { PetService } from "./pet.service";

@Component({
    templateUrl:"src/app.html"
})
export class AppComponent{
    msg:string;
    petCount:number;
    pets:Array<{family:string,name:string,price:number}>;

    constructor(){
        this.msg = "哈哈哈，测试牛腩";
        let petService:PetService=new PetService();
        this.petCount = petService.getPetCount();
        this.pets = petService.getPets();
    }
}