"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//src\main.ts 应用程序入口
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var app_module_1 = require("./app.module");
var platform = (0, platform_browser_dynamic_1.platformBrowserDynamic)();
platform.bootstrapModule(app_module_1.AppModule).catch(function (err) { return console.error("main.ts中出错：", err); });
;
//# sourceMappingURL=main.js.map