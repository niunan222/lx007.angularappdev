"use strict";
exports.__esModule = true;
exports.Car = void 0;
var Car = /** @class */ (function () {
    function Car() {
    }
    Car.prototype.run = function () {
        console.log(this.color + " \u8F66\u5728\u8DD1\u3002\u3002\u3002");
    };
    return Car;
}());
exports.Car = Car;
//# sourceMappingURL=car.js.map