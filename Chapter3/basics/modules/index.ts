//console.log("空气温度："+temperature); //访问air.js中的变量
import {temperature} from './sea';
console.log("海水温度："+temperature);  

import {temperature as humanTemperature, swim} from './human';
console.log("人体温度："+humanTemperature);
swim();

import * as  human from './human';
console.log("人体温度："+human.temperature);
human.swim();