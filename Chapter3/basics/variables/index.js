"use strict";
exports.__esModule = true;
var _loop_1 = function (i) {
    setTimeout(function () { console.log(i); }, 100 * i);
};
for (var i = 0; i < 10; i++) {
    _loop_1(i);
}
var student = "L";
var student = "Lcng";
var teacher = "刘娟娟";
//let teacher="张明"; 错误，关键字let不能定义重复的变量
//const E;  错误，常量必须在声明的同时被赋值
var PI = 3.14;
//PI = 3.142; 错误，常量不能重复赋值
var CIRLCLE = {
    radius: 12
};
CIRLCLE.radius = 24; //常量的属性可以正确赋值
console.log(CIRLCLE.radius);
function add(a, b) {
    return a + b;
}
//console.log(add(1,'2'));
console.log(add(1, 2));
//# sourceMappingURL=index.js.map