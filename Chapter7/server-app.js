//server-app.js 通过express创建node.js服务器应用程序 

var express=require('express');
var app = express();

//中间件配置，允许客户端请求服务端目录node_modules和src下的静态资源
app.use('/node_modules',express.static('node_modules'));
app.use('/src',express.static('src'));
app.use('/images',express.static('images'));

//路由配置，当客户端请求服务器端的任何路径（除上面定义的外），都返回目录Src下的HTML文件index.html
app.get('*',function(req,res){
    res.sendFile(__dirname + "/src/index.html");
});

//启动应用程序，并监听50424端口上的请求
var server = app.listen(50424,function(){
    var host = server.address().address;
    var port = server.address().port;
    console.log('当前应用程序正在监听 http://%s:%s', host,port);
})
