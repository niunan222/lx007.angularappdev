//src\main.ts 应用程序入口
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
 
import { AppModule } from "./app/app.module";

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule).catch(err => console.error("main.ts中出错：",err));;