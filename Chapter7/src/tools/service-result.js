"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericServiceResult = exports.ServiceResult = void 0;
// src\tools\service-result.ts 远程调接口返回的JSON
var ServiceResult = /** @class */ (function () {
    function ServiceResult() {
    }
    return ServiceResult;
}());
exports.ServiceResult = ServiceResult;
//包含有数据 data的类，继承上面的
var GenericServiceResult = /** @class */ (function (_super) {
    __extends(GenericServiceResult, _super);
    function GenericServiceResult() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return GenericServiceResult;
}(ServiceResult));
exports.GenericServiceResult = GenericServiceResult;
//# sourceMappingURL=service-result.js.map