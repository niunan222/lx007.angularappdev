// src\tools\service-result.ts 远程调接口返回的JSON
export class ServiceResult{
    success:boolean;
    message:string;
}

//包含有数据 data的类，继承上面的
export class GenericServiceResult<TData> extends ServiceResult{
    data:TData;
}