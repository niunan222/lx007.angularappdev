// src\carts\components\search.component.ts 商品查询组件
import { Component,Input, Output,EventEmitter } from "@angular/core";

import { Router } from "@angular/router";
import { state,style,transition,animate,trigger } from "@angular/animations";
 
@Component({
    selector:"ware-search",
    templateUrl: "../views/ware-search.html",
    styleUrls: ['../views/ware-search.css'],
    animations:[
      trigger('activeInactive',[//动画触发器
        state('active',style({opacity:1})), //状态active及相应的样式
        state('inactive',style({opacity:0.65})), //状态inactive及相应的样式
        transition('inactive <=> active',animate('0.3s ease-in')) //状态inactive过渡到状态active时，相应的样式过渡效果
      ])
    ],
    moduleId: module.id
})
export class WareSearchComponent {
 changesChecked:boolean = false; //标记初始变化监测是否已经执行过
  wareName:string;
  constructor(private router:Router){
    //一次事件循环后，初始变化监测已经执行完成
    let timeout = setTimeout(()=>{
      this.changesChecked = true;
      clearTimeout(timeout);
    })
  }

  @Input()
  searchBoxStyle:{[key:string]:string}

  @Output()
  onSearch:EventEmitter<string> = new EventEmitter<string>();

  search(){
    if(!this.wareName){return;}

    //触发搜索事件
    this.onSearch.emit(this.wareName);

    this.router.navigate(['/ware-search',{wareName:this.wareName}]);
  }
}