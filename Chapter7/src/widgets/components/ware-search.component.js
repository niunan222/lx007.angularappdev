"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WareSearchComponent = void 0;
// src\carts\components\search.component.ts 商品查询组件
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var animations_1 = require("@angular/animations");
var WareSearchComponent = /** @class */ (function () {
    function WareSearchComponent(router) {
        var _this = this;
        this.router = router;
        this.changesChecked = false; //标记初始变化监测是否已经执行过
        this.onSearch = new core_1.EventEmitter();
        //一次事件循环后，初始变化监测已经执行完成
        var timeout = setTimeout(function () {
            _this.changesChecked = true;
            clearTimeout(timeout);
        });
    }
    WareSearchComponent.prototype.search = function () {
        if (!this.wareName) {
            return;
        }
        //触发搜索事件
        this.onSearch.emit(this.wareName);
        this.router.navigate(['/ware-search', { wareName: this.wareName }]);
    };
    __decorate([
        (0, core_1.Input)(),
        __metadata("design:type", Object)
    ], WareSearchComponent.prototype, "searchBoxStyle", void 0);
    __decorate([
        (0, core_1.Output)(),
        __metadata("design:type", core_1.EventEmitter)
    ], WareSearchComponent.prototype, "onSearch", void 0);
    WareSearchComponent = __decorate([
        (0, core_1.Component)({
            selector: "ware-search",
            templateUrl: "../views/ware-search.html",
            styleUrls: ['../views/ware-search.css'],
            animations: [
                (0, animations_1.trigger)('activeInactive', [
                    (0, animations_1.state)('active', (0, animations_1.style)({ opacity: 1 })), //状态active及相应的样式
                    (0, animations_1.state)('inactive', (0, animations_1.style)({ opacity: 0.65 })), //状态inactive及相应的样式
                    (0, animations_1.transition)('inactive <=> active', (0, animations_1.animate)('0.3s ease-in')) //状态inactive过渡到状态active时，相应的样式过渡效果
                ])
            ],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], WareSearchComponent);
    return WareSearchComponent;
}());
exports.WareSearchComponent = WareSearchComponent;
//# sourceMappingURL=ware-search.component.js.map