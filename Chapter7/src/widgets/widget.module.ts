// src\widgets\widget.module.ts 部件模块

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule,Routes } from "@angular/router";
import { FormsModule } from "@angular/forms"; 
import { WareSearchComponent } from "./components/ware-search.component";
import { AuthDirective } from "./directives/auth.directive";

 

@NgModule({ 
    imports:[
        CommonModule, 
        FormsModule
    ],
    declarations: [ WareSearchComponent,AuthDirective],  
    exports:[WareSearchComponent,AuthDirective] 
 
})
export class WidgetModule {

}
