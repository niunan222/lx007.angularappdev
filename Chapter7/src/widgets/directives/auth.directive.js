"use strict";
//src\widgets\directives\auth.directive.ts 登录验证指令
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthDirective = void 0;
var core_1 = require("@angular/core");
var customer_name_service_1 = require("../../services/customer-name.service");
var AuthDirective = /** @class */ (function () {
    function AuthDirective(customerNameService) {
        var _this = this;
        this.customerNameService = customerNameService;
        //订阅登录客户姓名，并保存返回的订阅对象
        this.customerNameSubscription = this.customerNameService.subscribeCustomerName(function (x) {
            _this.customerName = x;
        });
    }
    Object.defineProperty(AuthDirective.prototype, "display", {
        //绑定给目标DOM元素的样式style的值
        get: function () {
            return this.customerName ? 'inherit' : 'none';
        },
        enumerable: false,
        configurable: true
    });
    //当指令实例被销毁时，注销当前指令实例的观察者
    AuthDirective.prototype.ngOnDestroy = function () {
        this.customerNameSubscription.unsubscribe();
    };
    AuthDirective = __decorate([
        (0, core_1.Directive)({
            selector: '[auth]',
            host: {
                '[style.display]': 'display'
            }
        }),
        __metadata("design:paramtypes", [customer_name_service_1.CustomerNameService])
    ], AuthDirective);
    return AuthDirective;
}());
exports.AuthDirective = AuthDirective;
//# sourceMappingURL=auth.directive.js.map