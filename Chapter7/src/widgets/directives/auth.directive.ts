//src\widgets\directives\auth.directive.ts 登录验证指令

import { Directive, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
 
import { CustomerNameService } from "../../services/customer-name.service";

@Directive({
    selector:'[auth]', 
    host:{
        '[style.display]':'display'
    }
})
export class AuthDirective implements OnDestroy{
    customerName:string;
    customerNameSubscription:Subscription;
    constructor(private customerNameService:CustomerNameService){
        //订阅登录客户姓名，并保存返回的订阅对象
        this.customerNameSubscription = this.customerNameService.subscribeCustomerName(x=>{
            this.customerName = x;
        })
    }
    //绑定给目标DOM元素的样式style的值
    get display():string{
        return this.customerName?'inherit':'none';
    }
    //当指令实例被销毁时，注销当前指令实例的观察者
    ngOnDestroy(): void {
        this.customerNameSubscription.unsubscribe();
    }
}