"use strict";
// src\carts\cart.module.ts 购物车领域模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WidgetModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ware_search_component_1 = require("./components/ware-search.component");
var auth_directive_1 = require("./directives/auth.directive");
var WidgetModule = /** @class */ (function () {
    function WidgetModule() {
    }
    WidgetModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule
            ],
            declarations: [ware_search_component_1.WareSearchComponent, auth_directive_1.AuthDirective],
            exports: [ware_search_component_1.WareSearchComponent, auth_directive_1.AuthDirective]
        })
    ], WidgetModule);
    return WidgetModule;
}());
exports.WidgetModule = WidgetModule;
//# sourceMappingURL=widget.module.js.map