//src\carts\components\cart.component.ts  购物车组件
import { Component } from "@angular/core";
import { CartWare } from "../models/cart-ware";
import { CartService } from "../models/cart.service";
import { Router } from "@angular/router";
import { CartTotalService } from "../../services/cart-total.service";
import { TipService } from "../../services/tip.service";
import { ConfirmService } from "../../services/confirm.service";
import { state,style,transition,animate,trigger,query,group, animateChild } from "@angular/animations";

@Component({
    templateUrl: "../views/cart.html",
    styleUrls: ['../views/cart.css'],
    moduleId: module.id,
    animations:[
        trigger('wareFlyOut',[//定义动画触发器，向右移动，高度变小直到消失
            transition('* => void',[
                style({transform:'translateX(0)',height:'*',overflow:'hidden'}), //过渡前的起始样式
                //样式过渡效果及过渡到的目标样式
                animate('0.3s ease-out',style({transform:'translateX(100%)',height:'0',overflow:'hidden'}))
            ])
        ]),
        trigger('cartShrink',[//购物车中最后一个商品删除时的动画
            transition(':leave', //表示离开时的过渡，等价于 * => void
                group([//将数组中的动画组成一组，使他们同时播放
                    query(':leave',animateChild()), //查找过渡到删除状态的子元素，并播放子元素上已经有的动画
                    style({  height:'*',overflow:'hidden'}), 
                    animate('0.3s ease-out',style({  height:'0',overflow:'hidden'}))   
                ]) 
            )
        ])
    ]
})
export class CartComponent {
 cartWares:Array<CartWare>;
 

 constructor(private cartService:CartService,private cartTotalService:CartTotalService,private tipService:TipService, private confirmService:ConfirmService){}

 ngOnInit():void{
    this.getWareList();
 }

 getWareList(){
    this.cartService.getWareList().subscribe(x=>{
        if(!x.success){
            this.tipService.tip(x.message)
            return;
        }
        this.cartWares = x.data;
    })
 }

 removeWare(id:number){
    this.confirmService.confirm({title:'删除',body:'确认要删除当前商品吗？'}).subscribe(x=>{
        if(!x){
            console.log("用户取消删除。");
            return;
        }
        this.cartService.removeWare(id).subscribe(x=>{
            if(x.success ==true){
                let index = this.cartWares.findIndex(y=>y.id ==id);
                this.cartWares.splice(index,1);
    
                //通知购物车商品总数改变
                this.cartTotalService.cartTotalSubject.next(x.data);
            }
            this.tipService.tip(x.message)
        })
    })
    
 }

 updateWareCount(ware:CartWare,count:any,event?:Event){
    count = parseInt(count);
    if(isNaN(count)||count<1){
        this.tipService.tip("非法数量");

        //恢复购物车原来的数量
        if(event){
            (event.target as HTMLInputElement).value = ware.count.toString();
        }
        return;
    }

    this.cartService.updateWareCouant(ware.wareId,count).subscribe(x=>{
        if(x.success==true){
            ware.count = count;
               //通知购物车商品总数改变
               this.cartTotalService.cartTotalSubject.next(x.data);
        }else{
            //恢复购物车原来的数量
            if(event){
                (event.target as HTMLInputElement).value = ware.count.toString();
            }
        }
       this.tipService.tip(x.message);
        return;
    })
 }

 //增加数量按钮
 increaseWareCount(ware:CartWare){
    this.updateWareCount(ware,ware.count+1);
 }

 //减少数量按钮
 decreaseWareCount(ware:CartWare){
    if(ware.count<2){
        return;
    }
    this.updateWareCount(ware,ware.count-1);
 }

 

 //监听搜索事件
 logSearch(wareName:string):void{
    console.log('搜索关键字：'+wareName);

    let waresInCart :string = this.cartWares.map(x=>x.wareName).join(',');
    console.log('购物车中的商品：'+waresInCart);
 }
}