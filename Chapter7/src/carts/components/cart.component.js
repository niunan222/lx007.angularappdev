"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartComponent = void 0;
//src\carts\components\cart.component.ts  购物车组件
var core_1 = require("@angular/core");
var cart_service_1 = require("../models/cart.service");
var cart_total_service_1 = require("../../services/cart-total.service");
var tip_service_1 = require("../../services/tip.service");
var confirm_service_1 = require("../../services/confirm.service");
var animations_1 = require("@angular/animations");
var CartComponent = /** @class */ (function () {
    function CartComponent(cartService, cartTotalService, tipService, confirmService) {
        this.cartService = cartService;
        this.cartTotalService = cartTotalService;
        this.tipService = tipService;
        this.confirmService = confirmService;
    }
    CartComponent.prototype.ngOnInit = function () {
        this.getWareList();
    };
    CartComponent.prototype.getWareList = function () {
        var _this = this;
        this.cartService.getWareList().subscribe(function (x) {
            if (!x.success) {
                _this.tipService.tip(x.message);
                return;
            }
            _this.cartWares = x.data;
        });
    };
    CartComponent.prototype.removeWare = function (id) {
        var _this = this;
        this.confirmService.confirm({ title: '删除', body: '确认要删除当前商品吗？' }).subscribe(function (x) {
            if (!x) {
                console.log("用户取消删除。");
                return;
            }
            _this.cartService.removeWare(id).subscribe(function (x) {
                if (x.success == true) {
                    var index = _this.cartWares.findIndex(function (y) { return y.id == id; });
                    _this.cartWares.splice(index, 1);
                    //通知购物车商品总数改变
                    _this.cartTotalService.cartTotalSubject.next(x.data);
                }
                _this.tipService.tip(x.message);
            });
        });
    };
    CartComponent.prototype.updateWareCount = function (ware, count, event) {
        var _this = this;
        count = parseInt(count);
        if (isNaN(count) || count < 1) {
            this.tipService.tip("非法数量");
            //恢复购物车原来的数量
            if (event) {
                event.target.value = ware.count.toString();
            }
            return;
        }
        this.cartService.updateWareCouant(ware.wareId, count).subscribe(function (x) {
            if (x.success == true) {
                ware.count = count;
                //通知购物车商品总数改变
                _this.cartTotalService.cartTotalSubject.next(x.data);
            }
            else {
                //恢复购物车原来的数量
                if (event) {
                    event.target.value = ware.count.toString();
                }
            }
            _this.tipService.tip(x.message);
            return;
        });
    };
    //增加数量按钮
    CartComponent.prototype.increaseWareCount = function (ware) {
        this.updateWareCount(ware, ware.count + 1);
    };
    //减少数量按钮
    CartComponent.prototype.decreaseWareCount = function (ware) {
        if (ware.count < 2) {
            return;
        }
        this.updateWareCount(ware, ware.count - 1);
    };
    //监听搜索事件
    CartComponent.prototype.logSearch = function (wareName) {
        console.log('搜索关键字：' + wareName);
        var waresInCart = this.cartWares.map(function (x) { return x.wareName; }).join(',');
        console.log('购物车中的商品：' + waresInCart);
    };
    CartComponent = __decorate([
        (0, core_1.Component)({
            templateUrl: "../views/cart.html",
            styleUrls: ['../views/cart.css'],
            moduleId: module.id,
            animations: [
                (0, animations_1.trigger)('wareFlyOut', [
                    (0, animations_1.transition)('* => void', [
                        (0, animations_1.style)({ transform: 'translateX(0)', height: '*', overflow: 'hidden' }), //过渡前的起始样式
                        //样式过渡效果及过渡到的目标样式
                        (0, animations_1.animate)('0.3s ease-out', (0, animations_1.style)({ transform: 'translateX(100%)', height: '0', overflow: 'hidden' }))
                    ])
                ]),
                (0, animations_1.trigger)('cartShrink', [
                    (0, animations_1.transition)(':leave', //表示离开时的过渡，等价于 * => void
                    (0, animations_1.group)([
                        (0, animations_1.query)(':leave', (0, animations_1.animateChild)()), //查找过渡到删除状态的子元素，并播放子元素上已经有的动画
                        (0, animations_1.style)({ height: '*', overflow: 'hidden' }),
                        (0, animations_1.animate)('0.3s ease-out', (0, animations_1.style)({ height: '0', overflow: 'hidden' }))
                    ]))
                ])
            ]
        }),
        __metadata("design:paramtypes", [cart_service_1.CartService, cart_total_service_1.CartTotalService, tip_service_1.TipService, confirm_service_1.ConfirmService])
    ], CartComponent);
    return CartComponent;
}());
exports.CartComponent = CartComponent;
//# sourceMappingURL=cart.component.js.map