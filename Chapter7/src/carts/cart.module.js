"use strict";
// src\carts\cart.module.ts 购物车领域模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var cart_component_1 = require("./components/cart.component");
var cart_service_1 = require("./models/cart.service");
var cart_routing_module_1 = require("./cart-routing.module");
var widget_module_1 = require("../widgets/widget.module");
var CartModule = /** @class */ (function () {
    function CartModule() {
    }
    CartModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                common_1.CommonModule,
                cart_routing_module_1.CartRoutingModule, //导入 领域路由配置模块 
                forms_1.FormsModule,
                widget_module_1.WidgetModule,
            ],
            declarations: [cart_component_1.CartComponent],
            providers: [
                cart_service_1.CartService //依赖注入 商品服务
            ]
        })
    ], CartModule);
    return CartModule;
}());
exports.CartModule = CartModule;
//# sourceMappingURL=cart.module.js.map