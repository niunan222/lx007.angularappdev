"use strict";
// src\carts\models\cart.service.ts 购物车服务类
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var CartService = /** @class */ (function () {
    function CartService(httpClient) {
        this.httpClient = httpClient;
    }
    CartService.prototype.getWareList = function () {
        return this.httpClient.get("http://localhost:5039/api/cart/list");
    };
    CartService.prototype.removeWare = function (id) {
        return this.httpClient.post("http://localhost:5039/api/cart/remove", { id: id });
    };
    CartService.prototype.updateWareCouant = function (wareId, count) {
        return this.httpClient.post("http://localhost:5039/api/cart/update", { wareId: wareId, count: count });
    };
    CartService = __decorate([
        (0, core_1.Injectable)() //标识为可被注入
        ,
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CartService);
    return CartService;
}());
exports.CartService = CartService;
//# sourceMappingURL=cart.service.js.map