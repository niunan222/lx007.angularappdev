// src\carts\models\cart-ware.ts 购物车商品实体类
export class CartWare{
    id:number;
    count:number;
    wareId:number;
    wareName:string;
    price:number;
    thumbnailUrl:string
}