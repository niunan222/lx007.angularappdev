// src\carts\models\cart.service.ts 购物车服务类
 
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Observable} from 'rxjs';
import { CartWare } from "./cart-ware";
import { GenericServiceResult } from "../../tools/service-result";

@Injectable() //标识为可被注入
export class CartService{
 
    constructor(private httpClient:HttpClient){}

    getWareList():Observable<GenericServiceResult<Array<CartWare>>>{
        return this.httpClient.get<GenericServiceResult<Array<CartWare>>>("http://localhost:5039/api/cart/list");
    }

    removeWare(id:number):Observable<GenericServiceResult<number>>{
        return this.httpClient.post<GenericServiceResult<number>>("http://localhost:5039/api/cart/remove",{id:id});
    }

    updateWareCouant(wareId:number,count:number):Observable<GenericServiceResult<number>>{
        return this.httpClient.post<GenericServiceResult<number>>("http://localhost:5039/api/cart/update",{wareId,count});
    }
 
}