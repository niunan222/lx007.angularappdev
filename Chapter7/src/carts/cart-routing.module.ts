// src\carts\cart-routing.module.ts 购物车领域路由配置模块
import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";

import { CartComponent } from "./components/cart.component";

//定义路由表
let routes = [
    { path: '', component: CartComponent }, 
];

@NgModule({
    imports: [ 
        RouterModule.forChild(routes),//导入路由器模块并注册路由表 
    ],
    exports:[RouterModule] //导出路由器模块，从而使导入当前模块的模块间接导入路由器模块RouterModule
})
export class CartRoutingModule {

}