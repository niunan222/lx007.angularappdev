// src\carts\cart.module.ts 购物车领域模块

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule,Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { CartComponent } from "./components/cart.component";
import { CartService } from "./models/cart.service";
import {CartRoutingModule} from './cart-routing.module';
import { WidgetModule } from "../widgets/widget.module";

@NgModule({ 
    imports:[
        CommonModule,
        CartRoutingModule,//导入 领域路由配置模块 
        FormsModule,
        WidgetModule,
    ],
    declarations: [CartComponent],  
    providers:[
        CartService  //依赖注入 商品服务
    ]
})
export class CartModule {

}
