//src\systemjs.config.js
(function(global){
    System.config({
        paths:{ //路径变量，书籍代码里有这个，实际上没有用，还报错
            "npm":'node_modules/'
        },
        map:{ //路径映射
            'rxjs':"node_modules/rxjs", //书籍代码里用的是npm:rxjs ，报错的
            '@angular/core':'node_modules/@angular/core/bundles/core.umd.js',
            '@angular/common':'node_modules/@angular/common/bundles/common.umd.js',
            '@angular/platform-browser':'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/compiler':'node_modules/@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser-dynamic':'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/router':'node_modules/@angular/router/bundles/router.umd.js',  
            '@angular/forms':'node_modules/@angular/forms/bundles/forms.umd.js'  ,
            '@angular/common/http':'node_modules/@angular/common/bundles/common-http.umd.js'  ,
            '@angular/platform-browser/animations':'node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
            '@angular/animations':'node_modules/@angular/animations/bundles/animations.umd.js',
            '@angular/animations/browser':'node_modules/@angular/animations/bundles/animations-browser.umd.js'
        },
        packages:{
            'src':{
                defaultExtension:'js'
            },
            'rxjs':{
                main:'index.js',
                defaultExtension:'js'
            },
            'rxjs/operators':{
                main:'index.js'
            }
        }
    });
})();