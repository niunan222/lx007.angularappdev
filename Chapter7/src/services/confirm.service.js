"use strict";
//src\services\confirm.service.ts 确认提示服务
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmService = void 0;
var rxjs_1 = require("rxjs");
var confirm_component_1 = require("./components/confirm.component");
var core_1 = require("@angular/core");
var ConfirmService = /** @class */ (function () {
    function ConfirmService(appRef, componentFactoryResolver, injector) {
        this.appRef = appRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.injector = injector;
        //创建 组件工厂
        this.confirmComponentFactory = this.componentFactoryResolver.resolveComponentFactory(confirm_component_1.ConfirmComponent);
    }
    ConfirmService.prototype.confirm = function (message) {
        var _this = this;
        //创建 组件 （引用）
        var confirmComponentRef = this.confirmComponentFactory.create(this.injector);
        //附加 视图到应用程序中
        this.appRef.attachView(confirmComponentRef.hostView);
        //附加与 提法视图相应的DOM元素互HTML BODY元素中
        var domElement = confirmComponentRef.location.nativeElement;
        document.body.appendChild(domElement);
        //获取 提示组件，并设置消息提示组件的文字属性
        var confirmComponent = confirmComponentRef.instance;
        confirmComponent.message = message;
        //创建接收用户选择结果的可观察对象，并在其被 注册观察者时，保存这个观察者的引用
        var subscriber;
        var confirmObservable = new rxjs_1.Observable(function (x) {
            subscriber = x;
        });
        //设置组件 的确认或取消按钮被 单击时需要调用的函数 
        confirmComponent.onChoose = function (yesOrNo) {
            //销毁并移除确认提示组件 视图
            _this.appRef.detachView(confirmComponentRef.hostView);
            confirmComponentRef.destroy();
            domElement.remove();
            //通知观察者用户单击的是“确认”还是“取消”
            if (subscriber) {
                subscriber.next(yesOrNo);
            }
        };
        //返回接收用户选择结果的可观察对象，以使调用方注册观察这个结果的观察者
        return confirmObservable;
    };
    ConfirmService = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [core_1.ApplicationRef, core_1.ComponentFactoryResolver, core_1.Injector])
    ], ConfirmService);
    return ConfirmService;
}());
exports.ConfirmService = ConfirmService;
//# sourceMappingURL=confirm.service.js.map