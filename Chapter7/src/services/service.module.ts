//src\services\service.module.ts 服务模块

import { NgModule } from "@angular/core";
import { CartTotalService } from "./cart-total.service";
import { CustomerNameService } from "./customer-name.service";
import { Authguard } from "./auth.guard";
 

@NgModule({
    providers:[CartTotalService,CustomerNameService,
        Authguard,//注册路由守卫 
    ] // 注册服务
})
export class ServiceModule{}