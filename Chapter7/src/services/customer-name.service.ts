// src\services\customer-name.service.ts 已登录客户姓名服务
import { Injectable } from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { GenericServiceResult } from "../tools/service-result";

@Injectable()
export class CustomerNameService{
    //登录客户姓名subject
    private customernameSubject:Subject<string> = new Subject();
    //注入httpclient
    constructor(private httpClient:HttpClient){}
    //注册接收登录客户姓名的通知方法
    subscribeCustomerName(next:(x:string)=>void):Subscription{
        return this.customernameSubject.subscribe(next);
    }
    //更新登录客户姓名
    updateCustomerName(){
        this.httpClient.get<GenericServiceResult<string>>("http://localhost:5039/api/customer/name").subscribe(x=>{
            if(x.success){
                this.customernameSubject.next(x.data);
            }
        })
    }
}