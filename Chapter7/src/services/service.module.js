"use strict";
//src\services\service.module.ts 服务模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceModule = void 0;
var core_1 = require("@angular/core");
var cart_total_service_1 = require("./cart-total.service");
var customer_name_service_1 = require("./customer-name.service");
var auth_guard_1 = require("./auth.guard");
var ServiceModule = /** @class */ (function () {
    function ServiceModule() {
    }
    ServiceModule = __decorate([
        (0, core_1.NgModule)({
            providers: [cart_total_service_1.CartTotalService, customer_name_service_1.CustomerNameService,
                auth_guard_1.Authguard, //注册路由守卫 
            ] // 注册服务
        })
    ], ServiceModule);
    return ServiceModule;
}());
exports.ServiceModule = ServiceModule;
//# sourceMappingURL=service.module.js.map