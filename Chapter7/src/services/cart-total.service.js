"use strict";
// src\services\cart-total.service.ts 购物车商品总数服务
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartTotalService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var http_1 = require("@angular/common/http");
var CartTotalService = /** @class */ (function () {
    function CartTotalService(httpClient) {
        this.httpClient = httpClient;
        this.cartTotalSubject = new rxjs_1.Subject();
    }
    CartTotalService.prototype.updateCartTotal = function () {
        var _this = this;
        this.httpClient.get("http://localhost:5039/api/cart/total-count").subscribe(function (x) {
            if (!x.success) {
                return;
            }
            _this.cartTotalSubject.next(x.data);
        });
    };
    CartTotalService = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CartTotalService);
    return CartTotalService;
}());
exports.CartTotalService = CartTotalService;
//# sourceMappingURL=cart-total.service.js.map