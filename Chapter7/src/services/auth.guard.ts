//src\services\auth.guard.ts 路由守护
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CanLoad, Route, Router, UrlSegment } from "@angular/router";
import { Observable } from "rxjs";
import { GenericServiceResult } from "../tools/service-result";
import {map} from 'rxjs/operators';

@Injectable()
export class Authguard implements CanLoad{
    customerName:string|undefined;
    constructor(private httpClient:HttpClient,private router:Router){}

    canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
        if(this.customerName){
            return true;
        }

        let response:Observable<GenericServiceResult<string>> = this.httpClient.get<GenericServiceResult<string>>("http://localhost:5039/api/customer/name");
        let success :Observable<boolean> = response.pipe(map(x=>{
            if(x.success){
                this.customerName = x.data;
            }else{
                //取不到姓名即未登录 
                this.customerName = undefined;
                this.router.navigate(['/customer/log-in']);
            }
            return x.success;
        }));

        return success;
    }
}