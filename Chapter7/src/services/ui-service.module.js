"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UiServiceModule = void 0;
//src\services\ui-service.module.ts UI服务模块
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var tip_service_1 = require("./tip.service");
var tip_component_1 = require("./components/tip.component");
var confirm_service_1 = require("./confirm.service");
var confirm_component_1 = require("./components/confirm.component");
var UiServiceModule = /** @class */ (function () {
    function UiServiceModule() {
    }
    UiServiceModule = __decorate([
        (0, core_1.NgModule)({
            imports: [common_1.CommonModule], //导入通用模块
            providers: [tip_service_1.TipService, confirm_service_1.ConfirmService], //注册消息提示服务
            declarations: [tip_component_1.TipComponent, confirm_component_1.ConfirmComponent], //声明消息提示组件
            entryComponents: [tip_component_1.TipComponent, confirm_component_1.ConfirmComponent] //设置消息提示组件 为入口组件
        })
    ], UiServiceModule);
    return UiServiceModule;
}());
exports.UiServiceModule = UiServiceModule;
//# sourceMappingURL=ui-service.module.js.map