"use strict";
//src\services\tip.service.ts 消息提示服务
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipService = void 0;
var tip_component_1 = require("./components/tip.component");
var core_1 = require("@angular/core");
var TipService = /** @class */ (function () {
    /**
     * 构造消息提示服务
     * @param appRef 注入就用程序实例
     * @param componentFactoryResolver 注入组件工厂解析器
     * @param injector 注入依赖注入器
     */
    function TipService(appRef, componentFactoryResolver, injector) {
        this.appRef = appRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.injector = injector;
        //创建消息提示组件的组件工厂
        this.tipComponentFactory = this.componentFactoryResolver.resolveComponentFactory(tip_component_1.TipComponent);
    }
    TipService.prototype.tip = function (message) {
        var _this = this;
        //创建消息提示组件 （引用）
        var tipComponentRef = this.tipComponentFactory.create(this.injector);
        //附加消息提示视图到应用程序中
        this.appRef.attachView(tipComponentRef.hostView);
        //附加与消息提法视图相应的DOM元素互HTML BODY元素中
        var domElement = tipComponentRef.location.nativeElement;
        document.body.appendChild(domElement);
        //获取消息提示组件，并设置消息提示组件的文字属性
        var tipComponent = tipComponentRef.instance;
        tipComponent.message = message;
        //销毁并移除消息提示组件视图
        setTimeout(function () {
            _this.appRef.detachView(tipComponentRef.hostView);
            tipComponentRef.destroy();
            domElement.remove();
        }, 1500);
    };
    TipService = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [core_1.ApplicationRef, core_1.ComponentFactoryResolver, core_1.Injector])
    ], TipService);
    return TipService;
}());
exports.TipService = TipService;
//# sourceMappingURL=tip.service.js.map