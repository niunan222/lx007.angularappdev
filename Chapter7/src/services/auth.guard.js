"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Authguard = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var operators_1 = require("rxjs/operators");
var Authguard = /** @class */ (function () {
    function Authguard(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
    }
    Authguard.prototype.canLoad = function (route, segments) {
        var _this = this;
        if (this.customerName) {
            return true;
        }
        var response = this.httpClient.get("http://localhost:5039/api/customer/name");
        var success = response.pipe((0, operators_1.map)(function (x) {
            if (x.success) {
                _this.customerName = x.data;
            }
            else {
                //取不到姓名即未登录 
                _this.customerName = undefined;
                _this.router.navigate(['/customer/log-in']);
            }
            return x.success;
        }));
        return success;
    };
    Authguard = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], Authguard);
    return Authguard;
}());
exports.Authguard = Authguard;
//# sourceMappingURL=auth.guard.js.map