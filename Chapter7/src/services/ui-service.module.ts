
//src\services\ui-service.module.ts UI服务模块
import { NgModule } from "@angular/core";

import { CommonModule } from "@angular/common";
import { TipService } from "./tip.service";
import { TipComponent } from "./components/tip.component";
import { ConfirmService } from "./confirm.service";
import { ConfirmComponent } from "./components/confirm.component";

@NgModule({
    imports:[CommonModule], //导入通用模块
    providers:[TipService,ConfirmService], //注册消息提示服务
    declarations:[TipComponent,ConfirmComponent], //声明消息提示组件
    entryComponents:[TipComponent,ConfirmComponent] //设置消息提示组件 为入口组件
})
export class UiServiceModule{}