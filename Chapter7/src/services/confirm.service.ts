//src\services\confirm.service.ts 确认提示服务

import { Observable,Subscriber } from "rxjs";
import { ConfirmComponent } from "./components/confirm.component";
import { ApplicationRef,ComponentFactory, ComponentFactoryResolver,ComponentRef, Injectable, Injector } from "@angular/core";

@Injectable()
export class ConfirmService{
    private confirmComponentFactory:ComponentFactory<ConfirmComponent>;

    constructor(private appRef:ApplicationRef, private componentFactoryResolver:ComponentFactoryResolver, private injector:Injector){
        //创建 组件工厂
        this.confirmComponentFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmComponent);
    }

    confirm(message:{title:string,body:string}):Observable<boolean>{
         //创建 组件 （引用）
         let confirmComponentRef:ComponentRef<ConfirmComponent> = this.confirmComponentFactory.create(this.injector);
         //附加 视图到应用程序中
         this.appRef.attachView(confirmComponentRef.hostView);
         //附加与 提法视图相应的DOM元素互HTML BODY元素中
         let domElement = confirmComponentRef.location.nativeElement;
         document.body.appendChild(domElement);
         //获取 提示组件，并设置消息提示组件的文字属性
         let confirmComponent:ConfirmComponent = confirmComponentRef.instance;
         confirmComponent.message = message;

         //创建接收用户选择结果的可观察对象，并在其被 注册观察者时，保存这个观察者的引用
         let subscriber:Subscriber<boolean>;
         let confirmObservable:Observable<boolean>= new Observable((x)=>{
            subscriber = x;
         })
         //设置组件 的确认或取消按钮被 单击时需要调用的函数 
         confirmComponent.onChoose = (yesOrNo:boolean) =>{
            //销毁并移除确认提示组件 视图
            this.appRef.detachView(confirmComponentRef.hostView);
            confirmComponentRef.destroy();
            domElement.remove();
            //通知观察者用户单击的是“确认”还是“取消”
            if(subscriber){
                subscriber.next(yesOrNo);
            }
         }
        //返回接收用户选择结果的可观察对象，以使调用方注册观察这个结果的观察者
         return confirmObservable;
    }
}