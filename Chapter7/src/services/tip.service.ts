//src\services\tip.service.ts 消息提示服务

 
import { TipComponent } from "./components/tip.component";
import { ApplicationRef,ComponentFactory, ComponentFactoryResolver,ComponentRef, Injectable, Injector } from "@angular/core";
 
@Injectable()
export class TipService{
    //消息提示组件工厂
    private tipComponentFactory:ComponentFactory<TipComponent>;
    /**
     * 构造消息提示服务
     * @param appRef 注入就用程序实例
     * @param componentFactoryResolver 注入组件工厂解析器
     * @param injector 注入依赖注入器
     */
    constructor(private appRef:ApplicationRef, private componentFactoryResolver:ComponentFactoryResolver, private injector:Injector){
        //创建消息提示组件的组件工厂
        this.tipComponentFactory = this.componentFactoryResolver.resolveComponentFactory(TipComponent);
    }

    tip(message:string):void{
        //创建消息提示组件 （引用）
        let tipComponentRef:ComponentRef<TipComponent> = this.tipComponentFactory.create(this.injector);
        //附加消息提示视图到应用程序中
        this.appRef.attachView(tipComponentRef.hostView);
        //附加与消息提法视图相应的DOM元素互HTML BODY元素中
        let domElement = tipComponentRef.location.nativeElement;
        document.body.appendChild(domElement);
        //获取消息提示组件，并设置消息提示组件的文字属性
        let tipComponent:TipComponent = tipComponentRef.instance;
        tipComponent.message = message;
        //销毁并移除消息提示组件视图
        setTimeout(()=>{
            this.appRef.detachView(tipComponentRef.hostView);
            tipComponentRef.destroy();
            domElement.remove();
        },1500)
    }
}