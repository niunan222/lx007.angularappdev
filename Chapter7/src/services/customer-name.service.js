"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerNameService = void 0;
// src\services\customer-name.service.ts 已登录客户姓名服务
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var http_1 = require("@angular/common/http");
var CustomerNameService = /** @class */ (function () {
    //注入httpclient
    function CustomerNameService(httpClient) {
        this.httpClient = httpClient;
        //登录客户姓名subject
        this.customernameSubject = new rxjs_1.Subject();
    }
    //注册接收登录客户姓名的通知方法
    CustomerNameService.prototype.subscribeCustomerName = function (next) {
        return this.customernameSubject.subscribe(next);
    };
    //更新登录客户姓名
    CustomerNameService.prototype.updateCustomerName = function () {
        var _this = this;
        this.httpClient.get("http://localhost:5039/api/customer/name").subscribe(function (x) {
            if (x.success) {
                _this.customernameSubject.next(x.data);
            }
        });
    };
    CustomerNameService = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CustomerNameService);
    return CustomerNameService;
}());
exports.CustomerNameService = CustomerNameService;
//# sourceMappingURL=customer-name.service.js.map