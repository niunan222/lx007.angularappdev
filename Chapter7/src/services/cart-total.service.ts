// src\services\cart-total.service.ts 购物车商品总数服务
 
import { Injectable } from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { GenericServiceResult } from "../tools/service-result";

@Injectable()
export class CartTotalService{
    cartTotalSubject:Subject<number> = new Subject();

    constructor(private httpClient:HttpClient){}

    updateCartTotal():void{
        this.httpClient.get<GenericServiceResult<number>>("http://localhost:5039/api/cart/total-count").subscribe(x=>{
            if(!x.success){return;}
            this.cartTotalSubject.next(x.data);
        })
    }
}