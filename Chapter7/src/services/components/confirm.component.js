"use strict";
//src\services\components\confirm.component.ts 确认提示组件 
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmComponent = void 0;
var core_1 = require("@angular/core");
var ConfirmComponent = /** @class */ (function () {
    function ConfirmComponent() {
    }
    ConfirmComponent.prototype.yes = function () {
        this.onChoose(true);
    };
    ConfirmComponent.prototype.no = function () {
        this.onChoose(false);
    };
    ConfirmComponent = __decorate([
        (0, core_1.Component)({
            template: "\n       <div class=\"layer\">\n            <div class=\"message\">\n                <div>{{message.title}}</div>\n                <div>{{message.body}}</div>\n                <div>\n                    <button (click)=\"yes()\">\u786E\u8BA4</button>\n                    <button (click)=\"no()\">\u53D6\u6D88</button>\n                </div>\n            </div>\n        </div>  \n    ",
            styles: ["      \n@keyframes message-show{\n    0%{\n        opacity: 0;\n    }\n    100%{\n        opacity: 1;\n    }\n}\n.layer{\n    animation:  message-show 0.3s;\n    top:0;\n    z-index:2000;\n    position: absolute;\n    width:100%;\n    height:100%;\n    background-color: rgba(50,50,50,0.2);\n}\n.message{\n    position:absolute;\n    z-index: 2001;\n    box-shadow: 0 0 10px rgba(233,150,122,0.3);\n    width:80%;\n    min-height:100px;\n    border-radius: 3px;\n    top:calc(50% - 50px);\n    left:10%;\n    background-color: white;\n    text-align: center;\n    border:1px solid rgba(233, 150, 122, 0.3)\n}\n.message>div:first-child{\n    margin:10px 0 20px 0;\n    padding-left: 10px;\n    text-align:left;\n    font-size:18px;\n    font-weight:bold;\n    border-bottom: 1px solid rgba(233, 150, 122, 0.3);\n}\n.message>div:last-child{\n    margin:10px 0 10px 0;\n    padding-top:10px;\n    font-size:14px;\n}\n.message>div:last-child>button{\n    padding:3px 10px;\n}\n.message>div:last-child>button:first-child{\n    margin-right:30px;\n    background:#f0c14b;\n    background:linear-gradient(to bottom,#f7dfa5,#f0c14b);\n    border-color:#f7dfa5;\n}\n"]
        })
    ], ConfirmComponent);
    return ConfirmComponent;
}());
exports.ConfirmComponent = ConfirmComponent;
//# sourceMappingURL=confirm.component.js.map