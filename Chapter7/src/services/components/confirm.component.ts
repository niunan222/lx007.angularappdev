//src\services\components\confirm.component.ts 确认提示组件 

import { Component } from "@angular/core";
import { template } from "@angular/core/src/render3";

@Component({
    template:`
       <div class="layer">
            <div class="message">
                <div>{{message.title}}</div>
                <div>{{message.body}}</div>
                <div>
                    <button (click)="yes()">确认</button>
                    <button (click)="no()">取消</button>
                </div>
            </div>
        </div>  
    `,
    styles:[`      
@keyframes message-show{
    0%{
        opacity: 0;
    }
    100%{
        opacity: 1;
    }
}
.layer{
    animation:  message-show 0.3s;
    top:0;
    z-index:2000;
    position: absolute;
    width:100%;
    height:100%;
    background-color: rgba(50,50,50,0.2);
}
.message{
    position:absolute;
    z-index: 2001;
    box-shadow: 0 0 10px rgba(233,150,122,0.3);
    width:80%;
    min-height:100px;
    border-radius: 3px;
    top:calc(50% - 50px);
    left:10%;
    background-color: white;
    text-align: center;
    border:1px solid rgba(233, 150, 122, 0.3)
}
.message>div:first-child{
    margin:10px 0 20px 0;
    padding-left: 10px;
    text-align:left;
    font-size:18px;
    font-weight:bold;
    border-bottom: 1px solid rgba(233, 150, 122, 0.3);
}
.message>div:last-child{
    margin:10px 0 10px 0;
    padding-top:10px;
    font-size:14px;
}
.message>div:last-child>button{
    padding:3px 10px;
}
.message>div:last-child>button:first-child{
    margin-right:30px;
    background:#f0c14b;
    background:linear-gradient(to bottom,#f7dfa5,#f0c14b);
    border-color:#f7dfa5;
}
`]
})
export class ConfirmComponent{
    message:{title:string,body:string};
    onChoose:(yesOrNo:boolean) =>void;
    yes():void{
        this.onChoose(true);
    }
    no():void{
        this.onChoose(false);
    }
}