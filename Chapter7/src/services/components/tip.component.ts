//src\services\components\tip.component.ts 消息提示组件 

import { Component, OnDestroy } from "@angular/core";
 

@Component({ 
    template:`
                <div class="message" *ngIf="message"> {{message}}  </div> 
            `,
    styles: [`
        @keyframes message-show{
            0%{
                opacity: 0;
            }
            100%{
                opacity: 1;
            }
        }
        .message{
            animation: message-show 0.5s;
            position: absolute; 
            width: 100px;
            height:60px;
             line-height:60px;
            border-radius: 3px;
            background-color: rgba(50,50,50,0.7);
            top:calc(50% - 30px); /*注意减号两边要各有一个空格*/
            left:calc(50% - 50px);
            color:white;
            font-size:14px;
           
            font-weight: bold;
            text-align: center; 
            z-index:2000
        }
        `], 
})
export class TipComponent {
    private _message:string|undefined;
     
    set message(value:string){
        this._message = value;
        //1.5秒后清除消息
        let timeout = setTimeout(()=>{
            clearTimeout(timeout);
            this._message = undefined;
        },1500);
    }

    get message():string|undefined{
        return this._message;
    }
}