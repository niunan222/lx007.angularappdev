"use strict";
//src\services\components\tip.component.ts 消息提示组件 
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipComponent = void 0;
var core_1 = require("@angular/core");
var TipComponent = /** @class */ (function () {
    function TipComponent() {
    }
    Object.defineProperty(TipComponent.prototype, "message", {
        get: function () {
            return this._message;
        },
        set: function (value) {
            var _this = this;
            this._message = value;
            //1.5秒后清除消息
            var timeout = setTimeout(function () {
                clearTimeout(timeout);
                _this._message = undefined;
            }, 1500);
        },
        enumerable: false,
        configurable: true
    });
    TipComponent = __decorate([
        (0, core_1.Component)({
            template: "\n                <div class=\"message\" *ngIf=\"message\"> {{message}}  </div> \n            ",
            styles: ["\n        @keyframes message-show{\n            0%{\n                opacity: 0;\n            }\n            100%{\n                opacity: 1;\n            }\n        }\n        .message{            animation: message-show 0.5s;\n            position: absolute; \n            width: 100px;\n            height:60px;\n            border-radius: 3px;\n            background-color: rgba(50,50,50,0.7);\n            top:calc(50% - 30px); /*\u6CE8\u610F\u51CF\u53F7\u4E24\u8FB9\u8981\u5404\u6709\u4E00\u4E2A\u7A7A\u683C*/\n            left:calc(50% - 50px);\n            color:white;\n            font-size:14px;\n            line-height:14px;\n            font-weight: bold;\n            text-align: center;\n            padding-top  : 23px;\n            z-index:2000\n        }\n        "],
        })
    ], TipComponent);
    return TipComponent;
}());
exports.TipComponent = TipComponent;
//# sourceMappingURL=tip.component.js.map