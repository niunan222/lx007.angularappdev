// src\app\app-routing.module.ts 根路由配置模块


import { NgModule } from "@angular/core";
 
import { RouterModule,Routes } from "@angular/router";
import { Authguard } from "../services/auth.guard";
 
 //定义路由表
let routes: Routes = [ 
    {path:'customer',loadChildren:'src/customers/customer.module#CustomerModule'},//延迟加载客户领域模块
    {path:'cart',loadChildren:'src/carts/cart.module#CartModule',canLoad:[Authguard]},//延迟加载购物车模块
    { path: '**', redirectTo:'' },
];

@NgModule({ 
    imports:[
        RouterModule.forRoot(routes),//导入路由器模块并注册路由表
    ],
    exports: [RouterModule],  
})
export class AppRoutingModule {

}
