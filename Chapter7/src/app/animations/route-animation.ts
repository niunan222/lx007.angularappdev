//src\app\animations\route-animation.ts 页面转场动画定义

import { state, style, transition, animate, trigger, query, group, animateChild } from "@angular/animations";

export let routeAnimation = trigger('routeAnimation',[
    transition('* <=> *',[ //商品列表视图和商品详情视图之间的导航动画
        style({position:'relative'}),//使router-outlet的容器DIV的position样式为relative
        group([
            query(':leave',[
                style({position:'absolute',top:0,left:0,width:'100%',height:'*',overflow:'hidden'}),
                animate('0.5s ease-out',style({position:'absolute',top:0,left:'100%',width:'100%',height:'calc(100vh - 120px)',overflow:'hidden'}))
            ],{optional:true}),
            query(":enter",[
                style({opacity:0.5}),
                animate('0.5s ease-in',style({opacity:1}))
            ],{optional:true})
        ])
    ])
])