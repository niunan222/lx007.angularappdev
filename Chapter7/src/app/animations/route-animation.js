"use strict";
//src\app\animations\route-animation.ts 页面转场动画定义
Object.defineProperty(exports, "__esModule", { value: true });
exports.routeAnimation = void 0;
var animations_1 = require("@angular/animations");
exports.routeAnimation = (0, animations_1.trigger)('routeAnimation', [
    (0, animations_1.transition)('* <=> *', [
        (0, animations_1.style)({ position: 'relative' }), //使router-outlet的容器DIV的position样式为relative
        (0, animations_1.group)([
            (0, animations_1.query)(':leave', [
                (0, animations_1.style)({ position: 'absolute', top: 0, left: 0, width: '100%', height: '*', overflow: 'hidden' }),
                (0, animations_1.animate)('0.5s ease-out', (0, animations_1.style)({ position: 'absolute', top: 0, left: '100%', width: '100%', height: 'calc(100vh - 120px)', overflow: 'hidden' }))
            ], { optional: true }),
            (0, animations_1.query)(":enter", [
                (0, animations_1.style)({ opacity: 0.5 }),
                (0, animations_1.animate)('0.5s ease-in', (0, animations_1.style)({ opacity: 1 }))
            ], { optional: true })
        ])
    ])
]);
//# sourceMappingURL=route-animation.js.map