"use strict";
//src\app.module.ts 根模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./components/app.component");
var app_routing_module_1 = require("./app-routing.module");
var service_module_1 = require("../services/service.module");
var ware_module_1 = require("../wares/ware.module");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/common/http");
var widget_module_1 = require("../widgets/widget.module");
var ui_service_module_1 = require("../services/ui-service.module");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                platform_browser_1.BrowserModule, //导入浏览器模块,
                ware_module_1.WareModule, //导入商品模块
                app_routing_module_1.AppRoutingModule, //导入根路由模块,注：必须放在最后
                http_1.HttpClientModule,
                service_module_1.ServiceModule, //导入服务模块
                ui_service_module_1.UiServiceModule, //导入UI服务模块
                widget_module_1.WidgetModule, //部件模块
                animations_1.BrowserAnimationsModule
            ],
            declarations: [app_component_1.AppComponent], //声明组件appcomponent为当前模块内的组件
            bootstrap: [app_component_1.AppComponent] //从组件appcomponent启动
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map