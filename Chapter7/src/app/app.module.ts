//src\app.module.ts 根模块

import { NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from "./components/app.component";
 import { AppRoutingModule } from "./app-routing.module";    
import { ServiceModule } from "../services/service.module";
import { WareModule } from "../wares/ware.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { WidgetModule } from "../widgets/widget.module";
import { UiServiceModule } from "../services/ui-service.module";

@NgModule({
    imports: [
        BrowserModule,//导入浏览器模块,
        WareModule, //导入商品模块
        AppRoutingModule,//导入根路由模块,注：必须放在最后
        HttpClientModule,
        ServiceModule, //导入服务模块
        UiServiceModule, //导入UI服务模块
        WidgetModule, //部件模块
        BrowserAnimationsModule 
    ],
    declarations: [AppComponent], //声明组件appcomponent为当前模块内的组件
    bootstrap: [AppComponent] //从组件appcomponent启动
})
export class AppModule {

}