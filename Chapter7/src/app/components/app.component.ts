//src\app.component.ts 根组件
import { routeAnimation } from "../animations/route-animation";
import { Component, OnInit } from "@angular/core";
 import { CartTotalService } from "../../services/cart-total.service";
 import { CustomerNameService } from "../../services/customer-name.service";
import { Customer } from "../../customers/models/customer";
import { RouterOutlet } from "@angular/router";

@Component({
    selector:"app-root",
    templateUrl:"../views/app.html",
    styleUrls:['../views/app.css'],
    moduleId:module.id,
    animations:[routeAnimation] //设置动画触发器
})
export class AppComponent implements OnInit{
    //购物车商品总数
    cartTotal:number = 0;
    //登录客户姓名
    customerName:string;
    //是否禁止路由动画
    routeAnimationDisabled:boolean = true;

    constructor(private cartTotalService:CartTotalService,private customerNameService:CustomerNameService)
    {
        //向购物车商品总数服务注册观察者，以接收商品总数通知
        this.cartTotalService.cartTotalSubject.subscribe(x=>{
            this.cartTotal = x;
        }) 

        //向登录客户姓名服务注册通知
        this.customerNameService.subscribeCustomerName(a=>{
            this.customerName = a;            
        })

        //一次事件循环后，启动路由动画
        let timeout = setTimeout(()=>{
            this.routeAnimationDisabled = false;
            clearTimeout(timeout);
        })
    }

    ngOnInit(): void {
        this.customerNameService.updateCustomerName();
        this.cartTotalService.updateCartTotal();
    }

    //获取目标视图的路径
    getPath(routerOutlet:RouterOutlet):boolean|string{
        //在路由动画被启动前返回False,以防止动画状态的过渡
        if(this.routeAnimationDisabled){
            return false;
        }
        //在目标路由激活之前返回false
        if(!routerOutlet.isActivated){
            return false;
        }
        //通过目标跌幅获取目标视图路径
        //路径中的斜杠/会被换成横杠-，冒号：会被去掉
        let path = routerOutlet.activatedRoute.pathFromRoot.map(x=>{
            return x.routeConfig && x.routeConfig.path?.replace(/\//g,'-').replace(/:/g,'')
        }).join('-');

        //返回目标视图的路径
        //其中商品列表页视图的路径会被换成empty，商品细节视图的路径会是ware-detail-id
        //其他视图的路径则分别会是cart, customer-log-in,customer-sign-up
        return path == '-'?"empty":path.replace(/^-?|-?$/g,'');
    }
}