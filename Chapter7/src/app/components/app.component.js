"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppComponent = void 0;
//src\app.component.ts 根组件
var route_animation_1 = require("../animations/route-animation");
var core_1 = require("@angular/core");
var cart_total_service_1 = require("../../services/cart-total.service");
var customer_name_service_1 = require("../../services/customer-name.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(cartTotalService, customerNameService) {
        var _this = this;
        this.cartTotalService = cartTotalService;
        this.customerNameService = customerNameService;
        //购物车商品总数
        this.cartTotal = 0;
        //是否禁止路由动画
        this.routeAnimationDisabled = true;
        //向购物车商品总数服务注册观察者，以接收商品总数通知
        this.cartTotalService.cartTotalSubject.subscribe(function (x) {
            _this.cartTotal = x;
        });
        //向登录客户姓名服务注册通知
        this.customerNameService.subscribeCustomerName(function (a) {
            _this.customerName = a;
        });
        //一次事件循环后，启动路由动画
        var timeout = setTimeout(function () {
            _this.routeAnimationDisabled = false;
            clearTimeout(timeout);
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        this.customerNameService.updateCustomerName();
        this.cartTotalService.updateCartTotal();
    };
    //获取目标视图的路径
    AppComponent.prototype.getPath = function (routerOutlet) {
        //在路由动画被启动前返回False,以防止动画状态的过渡
        if (this.routeAnimationDisabled) {
            return false;
        }
        //在目标路由激活之前返回false
        if (!routerOutlet.isActivated) {
            return false;
        }
        //通过目标跌幅获取目标视图路径
        //路径中的斜杠/会被换成横杠-，冒号：会被去掉
        var path = routerOutlet.activatedRoute.pathFromRoot.map(function (x) {
            var _a;
            return x.routeConfig && ((_a = x.routeConfig.path) === null || _a === void 0 ? void 0 : _a.replace(/\//g, '-').replace(/:/g, ''));
        }).join('-');
        //返回目标视图的路径
        //其中商品列表页视图的路径会被换成empty，商品细节视图的路径会是ware-detail-id
        //其他视图的路径则分别会是cart, customer-log-in,customer-sign-up
        return path == '-' ? "empty" : path.replace(/^-?|-?$/g, '');
    };
    AppComponent = __decorate([
        (0, core_1.Component)({
            selector: "app-root",
            templateUrl: "../views/app.html",
            styleUrls: ['../views/app.css'],
            moduleId: module.id,
            animations: [route_animation_1.routeAnimation] //设置动画触发器
        }),
        __metadata("design:paramtypes", [cart_total_service_1.CartTotalService, customer_name_service_1.CustomerNameService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map