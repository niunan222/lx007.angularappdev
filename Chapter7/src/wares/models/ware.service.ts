//src\app\ware.service.ts 商品服务类
import { Ware } from "./ware";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { GenericServiceResult } from "../../tools/service-result";

@Injectable()
export class WareService{
  
    constructor(private httpClient:HttpClient){
       
    }

    getWareList():Observable<GenericServiceResult<Array<Ware>>>{
         return this.httpClient.get<GenericServiceResult<Array<Ware>>>('http://localhost:5039/api/ware/list')
    }

    getWare(wareId:number):Observable<GenericServiceResult<Ware>>{
        return this.httpClient.get<GenericServiceResult<Ware>>('http://localhost:5039/api/ware/detail?wareId='+wareId);
    }

    addToCart(wareId:number,count:number):Observable<GenericServiceResult<number>>{
        return this.httpClient.post<GenericServiceResult<number>>("http://localhost:5039/api/cart/add",{wareId,count});
    }

    search(wareName:string):Observable<GenericServiceResult<Array<Ware>>>{
        return this.httpClient.get<GenericServiceResult<Array<Ware>>>("http://localhost:5039/api/ware/search?wareName="+wareName);
    }
}