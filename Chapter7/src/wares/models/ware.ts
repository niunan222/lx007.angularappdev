// src\app\ware.ts 商品实体类
export class Ware{
    id:number;
    name:string;//商品名
    description:string;//描述
    stock:number;//库存量
    price:number;//价格
    promotion:number; //1满额减价，2有买有赠，3满额返券，其他数字不促销
    addedTime:Date; //上架时间
    thumbnailUrl:string; //缩略图URL
    imageUrl:string; //正常图片URL
}