"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WareService = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var WareService = /** @class */ (function () {
    function WareService(httpClient) {
        this.httpClient = httpClient;
    }
    WareService.prototype.getWareList = function () {
        return this.httpClient.get('http://localhost:5039/api/ware/list');
    };
    WareService.prototype.getWare = function (wareId) {
        return this.httpClient.get('http://localhost:5039/api/ware/detail?wareId=' + wareId);
    };
    WareService.prototype.addToCart = function (wareId, count) {
        return this.httpClient.post("http://localhost:5039/api/cart/add", { wareId: wareId, count: count });
    };
    WareService.prototype.search = function (wareName) {
        return this.httpClient.get("http://localhost:5039/api/ware/search?wareName=" + wareName);
    };
    WareService = __decorate([
        (0, core_1.Injectable)(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], WareService);
    return WareService;
}());
exports.WareService = WareService;
//# sourceMappingURL=ware.service.js.map