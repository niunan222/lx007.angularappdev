"use strict";
// src\wares\ware.module.ts 商品领域模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WareModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ware_list_component_1 = require("./components/ware-list.component");
var ware_detail_component_1 = require("./components/ware-detail.component");
var ware_routing_module_1 = require("./ware-routing.module");
var ware_service_1 = require("./models/ware.service");
var widget_module_1 = require("../widgets/widget.module");
var WareModule = /** @class */ (function () {
    function WareModule() {
    }
    WareModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                common_1.CommonModule,
                ware_routing_module_1.WareRoutingModule, //导入商品领域路由配置模块
                forms_1.FormsModule,
                widget_module_1.WidgetModule
            ],
            declarations: [ware_list_component_1.WareListComponent, ware_detail_component_1.WareDetailComponent],
            providers: [
                { provide: ware_service_1.WareService, useClass: ware_service_1.WareService } //依赖注入 商品服务
            ]
        })
    ], WareModule);
    return WareModule;
}());
exports.WareModule = WareModule;
//# sourceMappingURL=ware.module.js.map