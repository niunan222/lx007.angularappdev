// src\wares\ware.module.ts 商品领域模块

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule,Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { WareListComponent } from "./components/ware-list.component";
import { WareDetailComponent } from "./components/ware-detail.component";
import { WareRoutingModule } from "./ware-routing.module";
import { WareService } from "./models/ware.service";
import { WidgetModule } from "../widgets/widget.module";
 

@NgModule({ 
    imports:[
        CommonModule,
        WareRoutingModule,//导入商品领域路由配置模块
        FormsModule,
        WidgetModule
    ],
    declarations: [WareListComponent,WareDetailComponent],  
    providers:[
        {provide:WareService,useClass:WareService}  //依赖注入 商品服务
    ]
})
export class WareModule {

}
