"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WareDetailComponent = void 0;
var ware_service_1 = require("../models/ware.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cart_total_service_1 = require("../../services/cart-total.service");
var tip_service_1 = require("../../services/tip.service");
var WareDetailComponent = /** @class */ (function () {
    //为当前组件绑定动画触发器viewAnimation
    // @HostBinding('@viewAnimation')
    // animateView = true;
    function WareDetailComponent(wareService, route, cartTotalService, tipService) {
        this.wareService = wareService;
        this.route = route;
        this.cartTotalService = cartTotalService;
        this.tipService = tipService;
        this.wareCount = 1;
    }
    WareDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var wareId = this.route.snapshot.params["id"];
        this.wareService.getWare(wareId).subscribe(function (x) {
            if (!x.success) {
                console.error(x.message);
                return;
            }
            _this.ware = x.data;
        });
    };
    WareDetailComponent.prototype.addToCart = function () {
        var _this = this;
        var tempid = 0;
        if (this.ware != undefined) {
            tempid = this.ware.id;
        }
        this.wareService.addToCart(tempid, this.wareCount).subscribe(function (x) {
            _this.tipService.tip(x.message);
            //发送购物车商品总数更新通知
            if (x.success) {
                _this.cartTotalService.cartTotalSubject.next(x.data);
            }
        });
    };
    WareDetailComponent = __decorate([
        (0, core_1.Component)({
            selector: "ware-detail",
            templateUrl: "../views/ware-detail.html",
            styleUrls: ['../views/ware-detail.css'],
            moduleId: module.id,
            // animations: [//定义动画
            //     trigger('viewAnimation', [
            //         transition(':leave',[
            //             style({ position: 'abaolute', zIndex: 100, top: '60px', left: 0, width: '100%', height: '*', overflow: 'hidden' }),
            //             animate('0.3s ease-out', style({ position: 'abaolute', zIndex: 100, top: '60px', left: 0, width: '100%', height: 'calc(100vh - 120px)', overflow: 'hidden' }))
            //         ]) 
            //     ])
            // ]
        }),
        __metadata("design:paramtypes", [ware_service_1.WareService, router_1.ActivatedRoute, cart_total_service_1.CartTotalService, tip_service_1.TipService])
    ], WareDetailComponent);
    return WareDetailComponent;
}());
exports.WareDetailComponent = WareDetailComponent;
//# sourceMappingURL=ware-detail.component.js.map