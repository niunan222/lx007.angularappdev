"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WareListComponent = void 0;
// src\app\ware-list.component.ts 商品列表组件
var router_1 = require("@angular/router");
var ware_service_1 = require("../models/ware.service");
var core_1 = require("@angular/core");
var WareListComponent = /** @class */ (function () {
    function WareListComponent(wareService, route) {
        var _this = this;
        this.route = route;
        this.wareService = wareService;
        //  this.wareName = this.route.snapshot.params["wareName"];
        //  this.wareName = this.route.snapshot.queryParams["wareName"]; URL里可以用?的方式来传参数 http://localhost:50424?wareName=西瓜
        //接收活动路由项封装的路由参数的变化通知
        this.route.params.subscribe(function (x) {
            _this.wareName = x["wareName"];
            _this.getWareList();
        });
    }
    WareListComponent.prototype.getWareList = function () {
        var _this = this;
        var res = this.wareName ? this.wareService.search(this.wareName) : this.wareService.getWareList();
        res.subscribe(function (x) {
            if (!x.success) {
                console.error(x.message);
                return;
            }
            _this.wares = x.data;
        });
    };
    WareListComponent = __decorate([
        (0, core_1.Component)({
            selector: "ware-list",
            templateUrl: "../views/ware-list.html",
            styleUrls: ['../views/ware-list.css'],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [ware_service_1.WareService, router_1.ActivatedRoute])
    ], WareListComponent);
    return WareListComponent;
}());
exports.WareListComponent = WareListComponent;
//# sourceMappingURL=ware-list.component.js.map