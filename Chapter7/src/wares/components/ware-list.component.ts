// src\app\ware-list.component.ts 商品列表组件
import { ActivatedRoute } from "@angular/router";
import { Ware } from "../models/ware";
import { WareService } from "../models/ware.service";
import { Component ,OnInit} from "@angular/core";

@Component({
    selector:"ware-list",
    templateUrl:"../views/ware-list.html",
    styleUrls:['../views/ware-list.css'],
    moduleId:module.id
})
export class WareListComponent{
    wares:Array<Ware>;
    private wareService:WareService;
    private wareName:string; //用于搜索

    constructor(wareService:WareService,private route:ActivatedRoute){ 
        this.wareService = wareService;
     //  this.wareName = this.route.snapshot.params["wareName"];
       //  this.wareName = this.route.snapshot.queryParams["wareName"]; URL里可以用?的方式来传参数 http://localhost:50424?wareName=西瓜
       //接收活动路由项封装的路由参数的变化通知
       this.route.params.subscribe(x=>{
        this.wareName = x["wareName"];
        this.getWareList();
       })
    }

    getWareList(){
        let res = this.wareName?this.wareService.search(this.wareName):this.wareService.getWareList();
       res.subscribe(x=>{
        if(!x.success){
            console.error(x.message);
            return;
        }

        this.wares = x.data;
       })
    }
}