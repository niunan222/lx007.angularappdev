// src\app\components\ware-detail.component.ts 商品详情组件 
import { Ware } from "../models/ware";
import { WareService } from "../models/ware.service";
import { Component, OnInit, HostBinding } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CartTotalService } from "../../services/cart-total.service";
import { TipService } from "../../services/tip.service";
import { state, style, transition, animate, trigger, query, group, animateChild } from "@angular/animations";

@Component({
    selector: "ware-detail",
    templateUrl: "../views/ware-detail.html",
    styleUrls: ['../views/ware-detail.css'],
    moduleId: module.id,
    // animations: [//定义动画
    //     trigger('viewAnimation', [
    //         transition(':leave',[
    //             style({ position: 'abaolute', zIndex: 100, top: '60px', left: 0, width: '100%', height: '*', overflow: 'hidden' }),
    //             animate('0.3s ease-out', style({ position: 'abaolute', zIndex: 100, top: '60px', left: 0, width: '100%', height: 'calc(100vh - 120px)', overflow: 'hidden' }))
    //         ]) 
    //     ])
    // ]
})
export class WareDetailComponent implements OnInit {

    ware: Ware; //需要被展示的商品
    wareCount: number; //打算购买

    //为当前组件绑定动画触发器viewAnimation
    // @HostBinding('@viewAnimation')
    // animateView = true;


    constructor(private wareService: WareService, private route: ActivatedRoute, private cartTotalService: CartTotalService, private tipService: TipService) {
        this.wareCount = 1;
    }

    ngOnInit() {
        let wareId: any = this.route.snapshot.params["id"];
        this.wareService.getWare(wareId).subscribe(x => {
            if (!x.success) {
                console.error(x.message);
                return;
            }
            this.ware = x.data;
        });
    }

    addToCart() {
        var tempid = 0;
        if (this.ware != undefined) {
            tempid = this.ware.id;
        }
        this.wareService.addToCart(tempid, this.wareCount).subscribe(x => {
            this.tipService.tip(x.message);

            //发送购物车商品总数更新通知
            if (x.success) {
                this.cartTotalService.cartTotalSubject.next(x.data);
            }
        })
    }
}