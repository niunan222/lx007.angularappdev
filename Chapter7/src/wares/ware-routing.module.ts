//src\wares\ware.module.ts 商品领域路由配置模块
import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";

import { WareListComponent } from "./components/ware-list.component";
import { WareDetailComponent } from "./components/ware-detail.component";

//定义路由表
let routes = [
    { path: '', component: WareListComponent },
    { path: 'ware-detail/:id', component: WareDetailComponent },
    { path: 'ware-search', component: WareListComponent },
];

@NgModule({
    imports: [ 
        RouterModule.forChild(routes),//导入路由器模块并注册路由表 
    ],
    exports:[RouterModule] //导出路由器模块，从而使导入当前模块的模块间接导入路由器模块RouterModule
})
export class WareRoutingModule {

}