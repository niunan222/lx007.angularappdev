"use strict";
// src\customers\customer.module.ts 客户领域模块
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var log_in_component_1 = require("./components/log-in.component");
var sign_up_component_1 = require("./components/sign-up.component");
var customer_service_1 = require("./models/customer.service");
var customer_routing_module_1 = require("./customer-routing.module");
var CustomerModule = /** @class */ (function () {
    function CustomerModule() {
    }
    CustomerModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                common_1.CommonModule,
                customer_routing_module_1.CustomerRoutingModule, //导入 领域路由配置模块
                forms_1.FormsModule, //导入表单模块
            ],
            declarations: [log_in_component_1.LogInComponent, sign_up_component_1.SignUpComponent],
            providers: [
                customer_service_1.CustomerService //依赖注入  服务
            ]
        })
    ], CustomerModule);
    return CustomerModule;
}());
exports.CustomerModule = CustomerModule;
//# sourceMappingURL=customer.module.js.map