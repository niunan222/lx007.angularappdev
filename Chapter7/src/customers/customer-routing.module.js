"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerRoutingModule = void 0;
//src\customers\customer-routing.module.ts 客户领域路由配置模块
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var sign_up_component_1 = require("./components/sign-up.component");
var log_in_component_1 = require("./components/log-in.component");
//定义路由表
var routes = [
    { path: 'sign-up', component: sign_up_component_1.SignUpComponent },
    { path: 'log-in', component: log_in_component_1.LogInComponent },
];
var CustomerRoutingModule = /** @class */ (function () {
    function CustomerRoutingModule() {
    }
    CustomerRoutingModule = __decorate([
        (0, core_1.NgModule)({
            imports: [
                router_1.RouterModule.forChild(routes), //导入路由器模块并注册路由表 
            ],
            exports: [router_1.RouterModule] //导出路由器模块，从而使导入当前模块的模块间接导入路由器模块RouterModule
        })
    ], CustomerRoutingModule);
    return CustomerRoutingModule;
}());
exports.CustomerRoutingModule = CustomerRoutingModule;
//# sourceMappingURL=customer-routing.module.js.map