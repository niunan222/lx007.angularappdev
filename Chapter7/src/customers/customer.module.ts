// src\customers\customer.module.ts 客户领域模块

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule,Routes } from "@angular/router";
import { LogInComponent } from "./components/log-in.component";

import { SignUpComponent } from "./components/sign-up.component";
import { CustomerService } from "./models/customer.service"; 
import { CustomerRoutingModule } from "./customer-routing.module";

@NgModule({ 
    imports:[
        CommonModule,
        CustomerRoutingModule,//导入 领域路由配置模块
        FormsModule, //导入表单模块
    ],
    declarations: [LogInComponent,SignUpComponent],  
    providers:[
        CustomerService  //依赖注入  服务
    ]
})
export class CustomerModule {

}
