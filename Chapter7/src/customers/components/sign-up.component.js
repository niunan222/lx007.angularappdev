"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignUpComponent = void 0;
//src\customers\components\sign-up.component.ts 客户注册组件 
var core_1 = require("@angular/core");
var customer_service_1 = require("../models/customer.service");
var router_1 = require("@angular/router");
var tip_service_1 = require("../../services/tip.service");
var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(customerService, router, tipService) {
        this.customerService = customerService;
        this.router = router;
        this.tipService = tipService;
        this.customer = {
            profile: {}
        };
    }
    //客户注册应用逻辑
    SignUpComponent.prototype.signUp = function () {
        var _this = this;
        if (!this.agree) {
            console.error('必须同意条款才能注册。');
            this.tipService.tip('必须同意条款才能注册。');
            return;
        }
        //使用httpclient
        var res = this.customerService.addCustomer(this.customer);
        res.subscribe(function (x) {
            var json = x;
            if (json.success == true) {
                _this.tipService.tip("注册成功");
                console.log("注册成功。");
                _this.router.navigate(['log-in', { phone: _this.customer.phone }]);
            }
            else {
                _this.tipService.tip("注册失败");
                console.error("注册失败。");
            }
        });
        // let success = this.customerService.addCustomer(this.customer);
        // if (success == true) {
        //     console.log("注册成功。");
        //     this.router.navigate(['log-in', { phone: this.customer.phone }]);
        // } else {
        //     console.error("注册失败。");
        // }
    };
    SignUpComponent = __decorate([
        (0, core_1.Component)({
            templateUrl: "../views/sign-up.html",
            styleUrls: ['../views/sign-up.css'],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [customer_service_1.CustomerService, router_1.Router, tip_service_1.TipService])
    ], SignUpComponent);
    return SignUpComponent;
}());
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=sign-up.component.js.map