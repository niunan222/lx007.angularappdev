//src\customers\components\sign-up.component.ts 客户注册组件 
import { Component } from "@angular/core";
import { Customer, Profile } from "../models/customer";
import { CustomerService } from "../models/customer.service";
import { Router } from "@angular/router";
import { TipService } from "../../services/tip.service";
@Component({
    templateUrl: "../views/sign-up.html",
    styleUrls: ['../views/sign-up.css'],
    moduleId: module.id
})
export class SignUpComponent {
    private customer: Customer;
    private agree: boolean; //是否同意使用条款及隐私声明


    constructor(private customerService: CustomerService, private router: Router, private tipService:TipService) {
        this.customer = <Customer>{
            profile: <Profile>{}
        }
    }

    //客户注册应用逻辑
    private signUp(): void {
        if (!this.agree) {
            console.error('必须同意条款才能注册。');
            this.tipService.tip('必须同意条款才能注册。');
            return;
        }
        //使用httpclient
        let res = this.customerService.addCustomer(this.customer);
        res.subscribe(x=>{
            let json = x as {success:boolean,message:string};
            if(json.success == true){
                this.tipService.tip("注册成功");
                console.log("注册成功。");
                this.router.navigate(['log-in', { phone: this.customer.phone }]);
            }else   {
                this.tipService.tip("注册失败");
                console.error("注册失败。");
            }
        })


        // let success = this.customerService.addCustomer(this.customer);
        // if (success == true) {
        //     console.log("注册成功。");
        //     this.router.navigate(['log-in', { phone: this.customer.phone }]);
        // } else {
        //     console.error("注册失败。");
        // }
    }
}