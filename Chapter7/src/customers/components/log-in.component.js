"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogInComponent = void 0;
//src\customers\components\log-in.component.ts 客户登录组件 
var core_1 = require("@angular/core");
var customer_1 = require("../models/customer");
var customer_service_1 = require("../models/customer.service");
var router_1 = require("@angular/router");
var customer_name_service_1 = require("../../services/customer-name.service");
var cart_total_service_1 = require("../../services/cart-total.service");
var tip_service_1 = require("../../services/tip.service");
var LogInComponent = /** @class */ (function () {
    function LogInComponent(customerService, route, router, customerNameService, cartTotalService, tipService) {
        this.customerService = customerService;
        this.route = route;
        this.router = router;
        this.customerNameService = customerNameService;
        this.cartTotalService = cartTotalService;
        this.tipService = tipService;
        this.customer = new customer_1.Customer();
        this.customer.phone = this.route.snapshot.params['phone'];
    }
    //客户登录应用逻辑
    LogInComponent.prototype.logIn = function () {
        var _this = this;
        console.log("进入logIn方法。");
        //使用httpclient
        this.customerService.checkCustomer(this.customer).subscribe(function (x) {
            if (x.success) {
                _this.tipService.tip('登录成功');
                console.log('登录成功。');
                _this.router.navigate(['/']);
                //发送登录客户姓名更新通知
                _this.customerNameService.updateCustomerName();
                _this.cartTotalService.updateCartTotal();
            }
            else {
                _this.tipService.tip('登录成功');
                console.error('登录失败.');
            }
        });
        // let exit = this.customerService.checkCustomer(this.customer);
        // if(exit == true){
        //     console.log('登录成功。');
        //     this.router.navigate(['/']);
        // }else{
        //     console.error('登录失败.');
        // }
    };
    LogInComponent = __decorate([
        (0, core_1.Component)({
            templateUrl: "../views/log-in.html",
            styleUrls: ['../views/log-in.css'],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [customer_service_1.CustomerService, router_1.ActivatedRoute, router_1.Router, customer_name_service_1.CustomerNameService, cart_total_service_1.CartTotalService, tip_service_1.TipService])
    ], LogInComponent);
    return LogInComponent;
}());
exports.LogInComponent = LogInComponent;
//# sourceMappingURL=log-in.component.js.map