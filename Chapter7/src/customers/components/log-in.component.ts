//src\customers\components\log-in.component.ts 客户登录组件 
import { Component } from "@angular/core";
import { Customer, Profile } from "../models/customer";
import { CustomerService } from "../models/customer.service";
import { Router,ActivatedRoute, ActivationEnd } from "@angular/router";
import { WareService } from "../../wares/models/ware.service";
import { CustomerNameService } from "../../services/customer-name.service";
import { CartTotalService } from "../../services/cart-total.service";
import { TipService } from "../../services/tip.service";

@Component({
    templateUrl: "../views/log-in.html",
    styleUrls: ['../views/log-in.css'],
    moduleId: module.id
})
export class LogInComponent {
    private customer: Customer;
    private shouldPersist:boolean; //是否需要记住客户的登录状态


    constructor(private customerService: CustomerService,private route:ActivatedRoute, private router: Router,private customerNameService:CustomerNameService ,private cartTotalService:CartTotalService,private tipService:TipService) {
        this.customer = new Customer();
        this.customer.phone = this.route.snapshot.params['phone'];
    }

    //客户登录应用逻辑
    private logIn(){
        console.log("进入logIn方法。");
        //使用httpclient
        this.customerService.checkCustomer(this.customer).subscribe(x=>{
            if(x.success){
                this.tipService.tip('登录成功');
                console.log('登录成功。');
                this.router.navigate(['/']);
                //发送登录客户姓名更新通知
                this.customerNameService.updateCustomerName();
                this.cartTotalService.updateCartTotal();
            }else{
                this.tipService.tip('登录成功');
                console.error('登录失败.');
            }
        })


        // let exit = this.customerService.checkCustomer(this.customer);
        // if(exit == true){
        //     console.log('登录成功。');

        //     this.router.navigate(['/']);
        // }else{
        //     console.error('登录失败.');
        // }
    }

   
}