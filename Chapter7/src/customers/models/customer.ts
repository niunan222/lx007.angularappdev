// src\customers\models\customer.ts 客户领域模型
export class Customer{
    id:number;
    name:string;
    phone:string;
    password:string;
    profile:Profile;
}

//客户简介类
export class Profile{
    email:string
}