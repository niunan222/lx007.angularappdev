"use strict";
// src\customers\models\customer.service.ts 客户服务类
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var CustomerService = /** @class */ (function () {
    //注入httpclient
    function CustomerService(httpClient) {
        this.httpClient = httpClient;
        this.customers = new Array();
    }
    //添加客户
    CustomerService.prototype.addCustomer = function (customer) {
        //使用httpclient访问接口
        var res = this.httpClient.post("http://localhost:5039/api/customer/signup", customer);
        return res;
        // if(!customer || !customer.name || !customer.password || !customer.phone){
        //     return false;
        // }
        // //防止重复添加用户
        // let exitC = this.customers.find(x=>x.phone = customer.phone);
        // if(exitC!=undefined){
        //     return false;
        // }
        // //设置新客户的ID
        // customer.id = this.customers.length>0?this.customers[this.customers.length-1].id+1:1;
        // this.customers.push(customer);
        // return true;
    };
    //检测用户提供的客户是否存在
    CustomerService.prototype.checkCustomer = function (customer) {
        //使用httpclient访问接口
        var res = this.httpClient.post('http://localhost:5039/api/customer/login', customer);
        return res;
        // let targetC = this.customers.find(x=>x.phone == customer.phone && x.password == customer.password);
        // if(targetC === undefined){
        //     return false; 
        // }
        // return true;
    };
    CustomerService = __decorate([
        (0, core_1.Injectable)() //标识为可被注入
        ,
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CustomerService);
    return CustomerService;
}());
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map