// src\customers\models\customer.service.ts 客户服务类
 
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Observable} from 'rxjs';
import { Customer } from "./customer";
import { ServiceResult } from "../../tools/service-result";

@Injectable() //标识为可被注入
export class CustomerService{
     private customers:Array<Customer>;

    //注入httpclient
    constructor(private httpClient:HttpClient){
       this.customers = new Array<Customer>();
    }

    //添加客户
    public addCustomer(customer:Customer):Observable<Object>{
        //使用httpclient访问接口
        let res = this.httpClient.post("http://localhost:5039/api/customer/signup",customer);
        return res;

        // if(!customer || !customer.name || !customer.password || !customer.phone){
        //     return false;
        // }
        // //防止重复添加用户
        // let exitC = this.customers.find(x=>x.phone = customer.phone);
        // if(exitC!=undefined){
        //     return false;
        // }

        // //设置新客户的ID
        // customer.id = this.customers.length>0?this.customers[this.customers.length-1].id+1:1;

        // this.customers.push(customer);
        // return true;
    }

    //检测用户提供的客户是否存在
    public checkCustomer (customer:Customer):Observable<ServiceResult>{
        //使用httpclient访问接口
        let res = this.httpClient.post<ServiceResult>('http://localhost:5039/api/customer/login',customer);
        return res;

        // let targetC = this.customers.find(x=>x.phone == customer.phone && x.password == customer.password);
        // if(targetC === undefined){
        //     return false; 
        // }
        // return true;
    }
 
}