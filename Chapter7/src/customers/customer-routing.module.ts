//src\customers\customer-routing.module.ts 客户领域路由配置模块
import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";

import { SignUpComponent } from "./components/sign-up.component";
import { LogInComponent } from "./components/log-in.component";

//定义路由表
let routes = [
    { path: 'sign-up', component: SignUpComponent },
    { path: 'log-in', component: LogInComponent },
];

@NgModule({
    imports: [ 
        RouterModule.forChild(routes),//导入路由器模块并注册路由表 
    ],
    exports:[RouterModule] //导出路由器模块，从而使导入当前模块的模块间接导入路由器模块RouterModule
})
export class CustomerRoutingModule {

}